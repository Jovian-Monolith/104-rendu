Module 104 Final blablabla Louis-Arnaud Heufke

Fonctionnement :
- Créer un environement virtuel venv
- Si Linux :
  Dans le fichier .env, utiliser un mot de passe vide (PASS_MYSQL="")
    
- Si Windows :
  Dans le fichier .env utiliser root comme mot de passe (PASS_MYSQL="")
        
- Lancer /APP/database/connect_db_context_manager.py
- Lancer /APP/zzzdemos/1_ImportationDumpSql.py
- Lancer /APP/zzzdemos/2_ConnectionBd.py
- Lancer /APP/1_run_server_flask.py
          
          - A l'aide du savoureux navigateur de votre choix, lancer l'URL : "http://127.0.0.1:5005/"
